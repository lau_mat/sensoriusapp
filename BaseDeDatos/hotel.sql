-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: hotel_db
-- ------------------------------------------------------
-- Server version	5.6.29-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_camas`
--

DROP TABLE IF EXISTS `tbl_camas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_camas` (
  `IdCama` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCuarto` bigint(20) DEFAULT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Status` varchar(20) DEFAULT 'ACTIVO',
  `rutaImg` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`IdCama`),
  KEY `cuarto_idx` (`IdCuarto`),
  CONSTRAINT `cuarto` FOREIGN KEY (`IdCuarto`) REFERENCES `tbl_cuartos` (`IdCuarto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_camas`
--

LOCK TABLES `tbl_camas` WRITE;
/*!40000 ALTER TABLE `tbl_camas` DISABLE KEYS */;
INSERT INTO `tbl_camas` VALUES (1,1,'cama1','SIN_ACTIVIDAD',NULL);
/*!40000 ALTER TABLE `tbl_camas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_cuartos`
--

DROP TABLE IF EXISTS `tbl_cuartos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cuartos` (
  `IdCuarto` bigint(20) NOT NULL AUTO_INCREMENT,
  `Numero` bigint(20) DEFAULT NULL,
  `Piso` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`IdCuarto`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_cuartos`
--

LOCK TABLES `tbl_cuartos` WRITE;
/*!40000 ALTER TABLE `tbl_cuartos` DISABLE KEYS */;
INSERT INTO `tbl_cuartos` VALUES (1,2,'1');
/*!40000 ALTER TABLE `tbl_cuartos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_personas`
--

DROP TABLE IF EXISTS `tbl_personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_personas` (
  `IdPersona` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombres` varchar(60) DEFAULT NULL,
  `Ap_Paterno` varchar(45) DEFAULT NULL,
  `AP_Materno` varchar(45) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_personas`
--

LOCK TABLES `tbl_personas` WRITE;
/*!40000 ALTER TABLE `tbl_personas` DISABLE KEYS */;
INSERT INTO `tbl_personas` VALUES (1,'carlos','laureano',NULL,'carlos.12'),(2,'roberto','mata',NULL,NULL);
/*!40000 ALTER TABLE `tbl_personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_registros`
--

DROP TABLE IF EXISTS `tbl_registros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_registros` (
  `IdRegistro` bigint(20) NOT NULL AUTO_INCREMENT,
  `Mac` varchar(30) DEFAULT NULL,
  `Hora` time DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`IdRegistro`),
  KEY `sensor_idx` (`Mac`),
  CONSTRAINT `sensor` FOREIGN KEY (`Mac`) REFERENCES `tbl_sensores` (`Mac`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_registros`
--

LOCK TABLES `tbl_registros` WRITE;
/*!40000 ALTER TABLE `tbl_registros` DISABLE KEYS */;
INSERT INTO `tbl_registros` VALUES (13,'60:01:94:10:35:C4','10:15:15','2017-01-13','SIN_ACTIVIDAD'),(14,'60:01:94:10:35:C4','10:15:43','2017-01-13','ACTIVIDAD'),(15,'60:01:94:10:35:C4','10:15:55','2017-01-13','SIN_ACTIVIDAD'),(16,'60:01:94:10:35:C4','10:16:46','2017-01-13','ACTIVIDAD'),(17,'60:01:94:10:35:C4','10:16:51','2017-01-13','SIN_ACTIVIDAD'),(18,'60:01:94:10:35:C4','11:03:20','2017-01-13','ACTIVIDAD'),(19,'60:01:94:10:35:C4','11:03:25','2017-01-13','SIN_ACTIVIDAD');
/*!40000 ALTER TABLE `tbl_registros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sensores`
--

DROP TABLE IF EXISTS `tbl_sensores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sensores` (
  `Mac` varchar(30) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `IdCama` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Mac`),
  KEY `cama_idx` (`IdCama`),
  CONSTRAINT `cama` FOREIGN KEY (`IdCama`) REFERENCES `tbl_camas` (`IdCama`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sensores`
--

LOCK TABLES `tbl_sensores` WRITE;
/*!40000 ALTER TABLE `tbl_sensores` DISABLE KEYS */;
INSERT INTO `tbl_sensores` VALUES ('5C:CF:7F:0B:03:0B','SensorPir',1),('60:01:94:10:35:C4','SensorPeso',1);
/*!40000 ALTER TABLE `tbl_sensores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sistema_usuarios_perfil`
--

DROP TABLE IF EXISTS `tbl_sistema_usuarios_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sistema_usuarios_perfil` (
  `IdPerfil` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(25) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `Control` varchar(15) DEFAULT NULL,
  `Error` varchar(15) DEFAULT NULL,
  `Administrador` varchar(15) DEFAULT NULL,
  `Supervisor` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`IdPerfil`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sistema_usuarios_perfil`
--

LOCK TABLES `tbl_sistema_usuarios_perfil` WRITE;
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` DISABLE KEYS */;
INSERT INTO `tbl_sistema_usuarios_perfil` VALUES (1,'Administrador','ACTIVO','true','true','true','true'),(2,'Supervisor','ACTIVO','true','true','NULL','true');
/*!40000 ALTER TABLE `tbl_sistema_usuarios_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usuarios`
--

DROP TABLE IF EXISTS `tbl_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usuarios` (
  `IdUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `Usuario` varchar(50) DEFAULT NULL,
  `Password` varchar(256) DEFAULT NULL,
  `IdPersona` bigint(20) DEFAULT NULL,
  `Status` varchar(20) DEFAULT 'PENDIENTE',
  `IdPerfil` bigint(20) NOT NULL,
  PRIMARY KEY (`IdUsuario`),
  KEY `persona_idx` (`IdPersona`),
  KEY `fk_tbl_usuarios_tbl_sistema_usuarios_perfil1_idx` (`IdPerfil`),
  CONSTRAINT `fk_tbl_usuarios_tbl_sistema_usuarios_perfil1` FOREIGN KEY (`IdPerfil`) REFERENCES `tbl_sistema_usuarios_perfil` (`IdPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `persona` FOREIGN KEY (`IdPersona`) REFERENCES `tbl_personas` (`IdPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usuarios`
--

LOCK TABLES `tbl_usuarios` WRITE;
/*!40000 ALTER TABLE `tbl_usuarios` DISABLE KEYS */;
INSERT INTO `tbl_usuarios` VALUES (1,'admin','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',1,'ACTIVO',1),(2,'superv','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',2,'ACTIVO',2);
/*!40000 ALTER TABLE `tbl_usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-06  1:49:12
