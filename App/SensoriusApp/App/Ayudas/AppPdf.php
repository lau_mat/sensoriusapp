<?php

class AppPdf{
    //variable privada y globar para esta clase
private $mpdf = false;

    /**
     * AppPdf constructor
     *
     * Inicializamos el  metodo require
     * y definimos el tamaño del pdf
     */
    function __construct() {
        $this->requireMpdf();
        $this->mpdf = new mPDF('c','A4');
    }

    /**
	 * Metodo Publico
	 * GenerarPdf($Html = false)
	 *
	 * Genera el PDF
	 * @return string
	 */
    public function GenerarPdf($Html = false){
        if ($Html == true AND isset($Html) == true):
            $this->mpdf->writeHTML($Html, 2);
            $Prefijo = substr(md5(uniqid(rand())), 0, 6);
            $Destino = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Temporales', $Prefijo . "_" . 'reporte.pdf'));
            $this->mpdf->Output($Destino, 'I');
        endif;
        unset($Html, $Prefijo, $Destino);
    }

    /**
     * Metodo Publico
     * EstablecerStyle($Style = false)
     *
     * Establecemos el estilo CSS que tendra nuestro reporte
     * @param bool $Style
     */
    public function EstablecerStyle($Style = false){
        if($Style == true AND isset($Style) == true):
            $this->mpdf->writeHTML($Style, 1);
        endif;
        unset($Style);
    }

    /**
     * Metodo Publico
     * requiereMpdf()
     *
     * Con este metodo cargamos mpdf.php
     */
    private function requireMpdf(){
        $Archivo = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'Mpdf', 'mpdf.php'));
        $Validacion = array_search($Archivo, get_included_files());
        if($Validacion == false AND is_numeric($Validacion) == false):
            if(file_exists($Archivo) == true):
                require_once $Archivo;
            else:
                throw new Excepcion('No se encontró el MPDF');
            endif;
        endif;
        unset($Archivo, $Validacion);
    }
}