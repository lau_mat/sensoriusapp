<?php
	/**
	 * Clase: Index_Modelo
	 */
	class Index_Modelo extends AppSQLConsultas {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico
		 * ConsultarUsuario($Usuario = false, $Password = false)
		 *
		 * Consulta los datos del usuario
		 * retorna un array asociativo con los datos correspondientes
		 * @param $Usuario: username
		 * @param $Password: contraseña
		 * @return array
		 **/
		public function ConsultarUsuario($Usuario = false, $Password = false) {
			if($Usuario == true AND $Password == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_personas');
				$Consulta->Columnas('Nombres, Ap_Paterno, AP_Materno, Email, Usuario, IdPerfil');
				$Consulta->InnerJoin('tbl_usuarios', 'tbl_personas.IdPersona', 'tbl_usuarios.IdPersona');
				$Consulta->Condicion("tbl_usuarios.Usuario = '$Usuario'");
				$Consulta->Condicion("tbl_usuarios.Password = '$Password'");
				$Consulta->Condicion("tbl_usuarios.Status = 'ACTIVO'");
				return $Consulta->Ejecutar(true, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarPermisos($Permisos = false)
		 *
		 * Genera la consulta de los datos correspondientes
		 * @param $Permiso: Identificador del permiso
		 * @return array
		 */
		public function ConsultarPermisos($Permisos = false) {
			if($Permisos == true AND is_numeric($Permisos) == true) {
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios_perfil');
				$Consulta->Columnas(self::ListarColumnas('tbl_sistema_usuarios_perfil', array('IdPerfil', 'Status'), false, APP));
				$Consulta->Condicion("IdPerfil = '$Permisos'");
				return $Consulta->Ejecutar(true, true);
			}
		}
	}