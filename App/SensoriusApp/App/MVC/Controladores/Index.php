<?php
	
	/**
	 * Clase: Index
	 */
	class Index extends Controlador {
		
		/**
		 * Metodo: Constructor
		 */
		function __Construct() {
			parent::__Construct();
			NeuralSesiones::Inicializar(APP);
			if(isset($_SESSION, $_SESSION['UOAUTH_APP']) == true){
				header('Location:'.NeuralRutasApp::RutaUrlAppModulo('Control'));
				exit();
			}
		}
		
		/**
		 * Metodo: Index
		 */
		public function Index() {
	        $this-> Login();		
	    }

		/**
		 * Metodo publico
		 *
		 * Login()
		 * Muestra el formulario para logearse al sistema.
		 * @throws NeuralException
		 */
		public function Login(){
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);

			$Validacion->Requerido('Usuario', '* Nombre de usuario requerido');
			$Validacion->Requerido('Password', '* Contraseña requerida');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Scripts', $Validacion->Constructor('frm_Login'));
			$Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
			echo $Plantilla->MostrarPlantilla('Index.html');
			unset($Validacion, $Plantilla);
			exit();
		}

		/**
		 * Index::Autenticacion()
		 *
		 * Genera el proceso de autenticacion
		 * @return void
		 */
		public function Autenticacion() {
			if(isset($_POST) == true AND isset($_POST['Key']) == true AND NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) :
				$this->AutenticacionDatosVacios();
			else:
			   exit('No se envio datos para gestionar');
			endif;
		}

		/**
		 * Index::AutenticacionDatosVacios()
		 *
		 * genera la validacion de datos vacios
		 * return ok
		 * @return void
		 */
		private function AutenticacionDatosVacios() {
			if(AppPost::DatosVacios($_POST) == false):
				$this->AutenticacionConsultarUsuario();
			else:
				exit('El formulario tiene datos vacios');
			endif;
		}

		/**
		 * Index::AutenticacionConsultarUsuario()
		 *
		 * Genera la validacion del usuario
		 * @return ok
		 * @return void
		 */
		private function AutenticacionConsultarUsuario() {
			unset($_POST['Key']);
			$DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
			$Consulta = $this->Modelo->ConsultarUsuario($DatosPost['Usuario'], hash('sha256', $DatosPost['Password']));
			if($Consulta['Cantidad'] == 1):
				$this->AutenticacionConsultaPermisos($Consulta);
			else:
			$this->AutenticacionErrorRedireccion('Error', 'SinAutorizacion');
			endif;
		}

		/**
		 * Index::AutenticacionConsultaPermisos()
		 *
		 * Genera la consulta de los permisos correspondientes
		 * @return ok
		 * @param bool $Consulta
		 * @return void
		 */
		private function AutenticacionConsultaPermisos($Consulta = false) {
			
			$ConsultaPermisos = $this->Modelo->ConsultarPermisos($Consulta[0]['IdPerfil']);
			if($ConsultaPermisos['Cantidad'] == 1):
				AppSession::Registrar($Consulta[0], $ConsultaPermisos[0]);
				header("Location: ".NeuralRutasApp::RutaUrlAppModulo('Control'));
				exit();
			else:
				header("Location: ".NeuralRutasApp::RutaUrlApp('LogOut'));
				exit();
			endif;
		}

		/**
		 * Index::AutenticacionErrorRedireccion()
		 *
		 * Genera el error de redireccion
		 * @return ok
		 * @param bool $modulo
		 * @param bool $controlador
		 * @param bool $metodo
		 * @return void
		 */
		private function AutenticacionErrorRedireccion($modulo = false, $controlador = false, $metodo = false) {
			header("Location: ".NeuralRutasApp::RutaUrlAppModulo($modulo, $controlador, $metodo));
			exit();
		}

	}