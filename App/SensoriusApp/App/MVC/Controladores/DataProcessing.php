<?php

/**
 * Clase: Index
 */
    class DataProcessing extends Controlador {

    /**
     * Metodo: Constructor
     */
    function __Construct() {
        parent::__Construct();
    }

    /**
     * Metodo: Index
     *  procesa los datos del sensor y almacena en DB
     */
    public function Index() {
        if(isset($_GET) == true && $_GET['Mac'] && $_GET['Estado']){
            $Mac=$_GET['Mac'];
            $StatusNew=$_GET['Estado'];

            if($StatusNew=='ACTIVIDAD'){
                $StatusNew='Ocupado';
                $this->Modelo->ProcesaDemo($Mac, $StatusNew);
            }else if($StatusNew=='SIN_ACTIVIDAD'){
                $StatusNew='Online';
                $this->Modelo->ProcesaDemo($Mac, $StatusNew);
            }
            unset($Mac,$StatusNew,$_GET);
            exit;
        }
    }
}