<?php
class Supervisor extends Controlador {

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct() {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index() {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['Ap_Paterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }


    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista todos Supervisores registrados en la db
     */
    public function frmListado(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            $Consulta = $this->Modelo->ConsultarSupervisores();
           $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function($Parametro){
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar un Supervisor.
     * @throws NeuralException
     */
    public function frmAgregar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            $Consulta=$this->Modelo->ConsultarHoteles();
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Usuario', '* Campo requerido');
            $Validacion->Requerido('Password', '* Campo requerido');
            $Validacion->Requerido('RepitePassword', '* Campo requerido');
            $Validacion->Requerido('IdHotel', '* Campo requerido');
            $Validacion->Requerido('Nombres','*Campo requerido');
            $Validacion->Requerido('Ap_Paterno', '* Campo requerido');
            $Validacion->Requerido('Ap_Materno', '* Campo requerido');
            $Validacion->Requerido('Email', '* Campo requerido');
            $Validacion->CantMaxCaracteres('Usuario',50, '* Máximo 50 caracteres');
            $Validacion->CantMaxCaracteres('Password',50, '* Máximo 50 caracteres');
            $Validacion->CantMaxCaracteres('RepitePassword',50, '* Máximo 50 caracteres');
            $Validacion->CantMaxCaracteres('Nombres',60,'*Máximo 60 caracteres');
            $Validacion->CantMaxCaracteres('Ap_Paterno',45, '* Máximo 45 Caracteres');
            $Validacion->CantMaxCaracteres('Ap_Materno',45, '* Máximo 45 Caracteres');
            $Validacion->CantMaxCaracteres('Email',100,'* Máximo 100 Caracteres');
            $Validacion->CampoIgual('RepitePassword','Password','* La contraseña no coincide con la anterior');
            $Validacion->Email('Email', '* Correo no válido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Hotel',$Consulta);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarSupervisor'));
            $Plantilla->Filtro('Cifrado', function($Parametro){
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Agregar', 'frmAgregarSupervisor.html')));
            unset($Validacion,$Plantilla);
            exit();
        }
    }

    /**
     * Metodo publico
     * Agregar()
     *
     * Metodo para registrar un nuevo Supervisor.
     * @throws NeuralException
     */
    public function Agregar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                $DatosPost = AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
                unset($_POST,$DatosPost['Key']);
                if($DatosPost['Password'] == $DatosPost['RepitePassword']){
                    if($this->Modelo->BuscarUsuario($DatosPost['Usuario']) == false){
                        $DatosPost = AppPost::FormatoEspacio(AppPost::ConvertirTextoUcwordsOmitido($DatosPost,array('Usuario','Password','RepitePassword','Email','Status','IdHotel')));
                        $DatosUsuario=array('IdPerfil'=> 2, 'Usuario'=>$DatosPost['Usuario'],'Password'=>hash('sha256',$DatosPost['Password']),'Status'=>$DatosPost['Status'], 'IdHotelAsignado'=>NeuralCriptografia::DeCodificar($DatosPost['IdHotel'], APP));
                        unset($DatosPost['Usuario'], $DatosPost['Password'], $DatosPost['RepitePassword'], $DatosPost['Status'], $DatosPost['IdHotel']);
                        $this->Modelo->InsertarUsuario($DatosUsuario, $this->Modelo->InsertarPersona($DatosPost));
                        unset($DatosPost,$DatosUsuario);
                        $Plantilla = new NeuralPlantillasTwig(APP);
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Agregar', 'Exito.html')));
                        unset($Plantilla);
                        exit();
                    }else{
                        $Plantilla = new NeuralPlantillasTwig(APP);
                        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Error', 'ErrorUsuario.html')));
                        unset($Plantilla);
                        exit();
                    }
                }else{
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Error', 'ErrorPassword.html')));
                    unset($Plantilla);
                    exit();
                }
            }else{
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Error', 'ErrorElementosRequeridos.html')));
                unset($Plantilla);
                exit();
            }
        }
    }

    public function Eliminar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if(isset($_POST)== true and $_POST['IdUsuario'] != ''){
                $IdUsuario= NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                $this->Modelo->Eliminar($IdUsuario);
            }
        }

    }
    /**
     * Metodo publico
     * frmEditarInstructor()
     *
     * Formulario para editar la informacion de un instructor.
     * @throws NeuralException
     */
    public function frmEditarSupervisor(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdUsuario']) == true AND $_POST['IdUsuario'] != '') {
                $IdUsuario=NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
                $ConsultaHoteles=$this->Modelo->ConsultarHoteles();
                unset($_POST);
                $DatosUsuario = $this->Modelo->ConsultarInformacionUsuario($IdUsuario);
                $ConsultaHoteles = AppUtilidades::ObtenerColumnaCoincidencia($ConsultaHoteles, $DatosUsuario, 'IdHotel');
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Validacion->Requerido('Usuario', '* Campo requerido');
                $Validacion->Requerido('Password', '* Campo requerido');
                $Validacion->Requerido('RepitePassword', '* Campo requerido');
                $Validacion->Requerido('IdHotel', '* Campo requerido');
                $Validacion->Requerido('Nombres','*Campo requerido');
                $Validacion->Requerido('Ap_Paterno', '* Campo requerido');
                $Validacion->Requerido('Ap_Materno', '* Campo requerido');
                $Validacion->Requerido('Email', '* Campo requerido');
                $Validacion->CantMaxCaracteres('Usuario',50, '* Máximo 50 caracteres');
                //$Validacion->CantMaxCaracteres('Password',50, '* Máximo 50 caracteres');
               // $Validacion->CantMaxCaracteres('RepitePassword',50, '* Máximo 50 caracteres');
                $Validacion->CantMaxCaracteres('Nombres',60,'*Máximo 60 caracteres');
                $Validacion->CantMaxCaracteres('Ap_Paterno',45, '* Máximo 45 Caracteres');
                $Validacion->CantMaxCaracteres('Ap_Materno',45, '* Máximo 45 Caracteres');
                $Validacion->CantMaxCaracteres('Email',100,'* Máximo 100 Caracteres');
                $Validacion->CampoIgual('RepitePassword','Password','* La contraseña no coincide con la anterior');
                $Validacion->Email('Email', '* Correo no válido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta',$DatosUsuario);
                $Plantilla->Parametro('ConsultaHoteles',$ConsultaHoteles);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarSupervisor'));
                $Plantilla->Filtro('Cifrado',function($parametros){return NeuralCriptografia::Codificar($parametros, APP);});
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Editar', 'frmEditarSupervisor.html')));
            }
        }

    }

    /**
     * Metodo publico
     * Actualizar()
     * Prepara los datos para editar la informacion del usuario
     * y hace la llamada a dicho metodo en el modelo
     */
    public function Editar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                $DatosPost = AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
                unset($_POST,$DatosPost['Key'],$DatosPost['RepitePassword']);
                $DatosPost['Status'] = (isset($DatosPost['Status'])==true) ? 'ACTIVO': 'DESACTIVADO';
                $IdPersona = NeuralCriptografia::DeCodificar($DatosPost['IdPersona'],APP);
                $IdUsuario = NeuralCriptografia::DeCodificar($DatosPost['IdUsuario'],APP);
                if(strlen($DatosPost['Password'])< 30){
                    $DatosEstado = array('Status' => $DatosPost['Status'],'Usuario' => $DatosPost['Usuario'],'Password' => hash('sha256',$DatosPost['Password']),'IdHotelAsignado'=>NeuralCriptografia::DeCodificar($DatosPost['IdHotel'], APP));
                }else{
                    $DatosEstado = array('Status' => $DatosPost['Status'],'Usuario' => $DatosPost['Usuario'], 'IdHotelAsignado'=>NeuralCriptografia::DeCodificar($DatosPost['IdHotel'], APP));
                }

                unset($DatosPost['Status'], $DatosPost['Usuario'],$DatosPost['Password'],$DatosPost['IdPersona'], $DatosPost['IdUsuario'], $DatosPost['IdHotel']);
                $this->Modelo->ActualizarDatosUsuario($DatosEstado, $IdUsuario);
                $this->Modelo->ActualizarDatosPersona($DatosPost, $IdPersona);

                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Editar', 'Exito.html')));
                unset($Plantilla);
                exit();
            }
        }
    }

}