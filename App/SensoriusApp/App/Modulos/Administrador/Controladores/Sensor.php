<?php
class Sensor extends Controlador {

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct() {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();

    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index() {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['Ap_Paterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }


    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista todos Sensores registrados en la db
     */
    public function frmListado(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            $Consulta = $this->Modelo->ConsultarSensores();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function($Parametro){
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar un Sensor.
     * @throws NeuralException
     */
    public function frmAgregar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Piso = $this->Modelo->ConsultarPiso();
            $Validacion->Requerido('Mac', '* Campo requerido');
            $Validacion->Requerido('Nombre', '* Campo requerido');
            $Validacion->Requerido('IdCama', '* Campo requerido');
            $Validacion->CantMaxCaracteres('Mac',17, '* No corresponde a una dirección Mac utilice : como separador');
            $Validacion->CantMinCaracteres('Mac',17, '* No corresponde a una dirección Mac utilice : como separador');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Piso', $Piso);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarSensor'));
            $Plantilla->Filtro('Cifrado', function($Parametro){
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Agregar', 'frmAgregarSensor.html')));
            unset($Validacion,$Plantilla,$Piso);
            exit();
        }
    }

    /**
     * Metodo publico
     * Agregar()
     *
     * Metodo para registrar un nuevo Sensor.
     * @throws NeuralException
     */
    public function Agregar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                $DatosPost = AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
                unset($_POST, $DatosPost['Key'], $DatosPost['Piso'], $DatosPost['IdCuarto']);
                $DatosPost['IdCama'] = NeuralCriptografia::DeCodificar($DatosPost['IdCama'], APP);
                $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($DatosPost));
                $this->Modelo->InsertarSensor($DatosPost);
                unset($DatosPost);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Agregar', 'Exito.html')));
                unset($Plantilla);
                exit();
            }else{
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Error', 'ErrorElementosRequeridos.html')));
                unset($Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo publico
     * ObtenerNumeroCuartos()
     *
     * Nos devuelve los numeros de los cuarto de dicho piso.
     * @throws NeuralException
     */
    public function ObtenerNumeroCuartos(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['Piso'])) {
                $Cuartos = $this->Modelo->ConsultarCuartos(NeuralCriptografia::DeCodificar($_POST['Piso'], APP));
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Cuarto', $Cuartos);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Agregar', 'SelectCuarto.html')));
                unset($Cuartos, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo publico
     * ObtenerCamasCuarto()
     *
     * Nos devuelve las camas del cuarto seleccionado.
     * @throws NeuralException
     */
    public function ObtenerCamasCuarto(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdCuarto'])) {
                $Camas = $this->Modelo->ConsultarCamas(NeuralCriptografia::DeCodificar($_POST['IdCuarto'], APP));
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Cama', $Camas);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Agregar', 'SelectCama.html')));
                unset($Camas, $Plantilla);
                exit();
            }
        }
    }


    /**
     * Metodo publico
     * Eliminar()
     *
     * Elimina el sensor con la Mac asociada.
     * @throws NeuralException
     */
    public function Eliminar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if(isset($_POST)== true and $_POST['Mac'] != ''){
                $Mac= NeuralCriptografia::DeCodificar($_POST['Mac'], APP);
                $this->Modelo->Eliminar($Mac);
            }
        }

    }


    /**
     * Metodo publico
     * frmEditar()
     *
     * Formulario para editar un Sensor.
     * @throws NeuralException
     */
    public function frmEditar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if (isset($_POST) == true and $_POST['Mac'] != '') {
                $Mac= NeuralCriptografia::DeCodificar($_POST['Mac'], APP);
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Piso = $this->Modelo->ConsultarPiso();
                $Consulta = $this->Modelo->ConsultarSensorUnico($Mac);
                $Piso = AppUtilidades::ObtenerColumnaCoincidencia($Piso, $Consulta, 'Piso');
                $auxPiso = $Consulta[0]['Piso'];
                $Cuarto = $this->Modelo->ConsultarCuartos($auxPiso);
                $Cuarto = AppUtilidades::ObtenerColumnaCoincidencia($Cuarto, $Consulta, 'IdCuarto');
                $auxCamas = $Consulta[0]['IdCuarto'];
                $Camas = $this->Modelo->ConsultarCamas($auxCamas);
                $Camas = AppUtilidades::ObtenerColumnaCoincidencia($Camas, $Consulta, 'IdCama');
                $Validacion->Requerido('Mac', '* Campo requerido');
                $Validacion->Requerido('Nombre', '* Campo requerido');
                $Validacion->Requerido('IdCama', '* Campo requerido');
                $Validacion->CantMaxCaracteres('Mac', 17, '* No corresponde a una dirección Mac utilice : como separador');
                $Validacion->CantMinCaracteres('Mac', 17, '* No corresponde a una dirección Mac utilice : como separador');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('MacOriginal', $Mac);
                $Plantilla->Parametro('Piso', $Piso);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('IdCuarto', $Cuarto);
                $Plantilla->Parametro('IdCama', $Camas);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgfrmEditarSensor'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Editar', 'frmEditarSensor.html')));
                unset($Validacion, $Plantilla, $Piso);
                exit();
            }
        }
    }

    /**
     * Metodo publico
     * Editar()
     *
     * Metodo para modificar un Sensor seleccionado.
     * @throws NeuralException
     */
    public function Editar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                $DatosPost = AppPost::LimpiarInyeccionSQL(AppPost::FormatoEspacio($_POST));
                $DatosPost['IdCama'] = NeuralCriptografia::DeCodificar($DatosPost['IdCama'], APP);
                unset($_POST, $DatosPost['Piso'], $DatosPost['IdCuarto'], $DatosPost['Key']);
                if($DatosPost['MacOriginal']==$DatosPost['Mac']){
                    $this->Modelo->ActualizaSensor(array('Nombre' => $DatosPost['Nombre'], 'IdCama'=>$DatosPost['IdCama']),  array('Mac'=>$DatosPost['Mac']));
                }else{
                    $Mac = $DatosPost['MacOriginal'];
                    unset($DatosPost['MacOriginal']);
                    $this->Modelo->Eliminar($Mac);
                    $this->Modelo->InsertarSensor($DatosPost);
                }
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Editar', 'Exito.html')));
                unset($Plantilla);
                exit();
            }else{
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Error', 'ErrorElementosRequeridos.html')));
                unset($Plantilla);
                exit();
            }
        }
    }

    public function ObtenerCamasCuartoEdit(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['IdCuarto'])) {
                $Camas = $this->Modelo->ConsultarCamas(NeuralCriptografia::DeCodificar($_POST['IdCuarto'], APP));
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Cama', $Camas);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Editar', 'SelectCama.html')));
                unset($Camas, $Plantilla);
                exit();
            }
        }
    }

    public function ObtenerNumeroCuartosEdit(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true AND isset($_POST['Piso'])) {
                $Cuartos = $this->Modelo->ConsultarCuartos(NeuralCriptografia::DeCodificar($_POST['Piso'], APP));
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Cuarto', $Cuartos);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Sensor', 'Editar', 'SelectCuarto.html')));
                unset($Cuartos, $Plantilla);
                exit();
            }
        }
    }
}