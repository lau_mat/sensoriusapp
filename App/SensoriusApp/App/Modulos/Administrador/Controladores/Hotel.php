<?php
class Hotel extends Controlador
{

    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();

    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['Ap_Paterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista todos Hoteles Registrados en la db
     */
    public function frmListado(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            $Consulta = $this->Modelo->ConsultarHoteles();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function($Parametro){
                return NeuralCriptografia::Codificar($Parametro, APP);
            });

          echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);

            exit();
        }
    }

    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista todos Sensores registrados en la db
     */
    public function frmListadoCuartos()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['IdHotel']) == true) {
                $IdHotel=NeuralCriptografia::DeCodificar($_POST['IdHotel'], APP);
                $Consulta = $this->Modelo->ConsultarCuartos($IdHotel);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('IdHotel', $IdHotel);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel','Cuartos', 'Listado', 'Listado.html')));
                unset($Consulta, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * ConsultaDiasHorario()
     *
     * Lista el horario del taller
     * @throws NeuralException
     */
    public function ConsultaCamas()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultarCamas(NeuralCriptografia::DeCodificar($_POST['IdCuarto'], APP));
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('ConsultaCamas', $Consulta);
            if (isset($Consulta) == true AND is_array($Consulta) == true AND count($Consulta) > 0) {
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel','Cuartos', 'Modal', 'TablaCamas.html')));
                unset($Consulta, $Plantilla);
                exit;
            } else {
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel','Cuartos', 'Modal', 'SinCamas.html')));
                unset($Consulta, $Plantilla);
                exit;
            }
        }
    }


    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar un Hotel.
     * @throws NeuralException
     */
    public function frmAgregar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Nombre', '* Campo requerido');
            $Validacion->Requerido('Direccion', '* Campo requerido');
           $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarHotel'));
            $Plantilla->Filtro('Cifrado', function($Parametro){
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Agregar', 'frmAgregarHotel.html')));
            unset($Validacion,$Plantilla,$Piso);
            exit();
        }
    }
    /**
     * Metodo publico
     * Agregar()
     *
     * Metodo para registrar un nuevo Hotel.
     * @throws NeuralException
     */
    public function Agregar(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
            if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true ){
                unset($_POST['Key']);
                $DatosPost = $_POST;
                $DatosPost['RutaWebImagen'] = NeuralCriptografia::DeCodificar($_POST['RutaWebImagen'], APP);
                $this->Modelo->GuardarHotel($DatosPost);
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Agregar', 'Exito.html')));
                unset($Plantilla);
                exit();
            }else{
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Error', 'ErrorElementosRequeridos.html')));
                unset($Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo publico
     * SubirImagen()
     *
     * Guarda la imagen en el servidor y retorna un input al formulario.
     * @throws NeuralException
     */
    public function SubirImagen(){
        $Archivo = $_FILES['Imagen'];
        $RutaPrincipal = 'imagenes'. DIRECTORY_SEPARATOR. 'logos';
        $RutaCompleta = AppArchivos::CargarArchivoImagen($Archivo, $RutaPrincipal);
        echo json_encode(array('RutaCifrada'=>NeuralCriptografia::Codificar($RutaCompleta,APP)));
        unset($Archivo, $RutaPrincipal, $RutaCompleta);
    }

    /**
     * Metodo publico
     * frmAgregar()
     *
     * Formulario para agregar taller.
     * @throws NeuralException
     */
    public function frmAgregarCuarto()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $IdHotel = NeuralCriptografia::DeCodificar($_POST['IdHotel'], APP);
            $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
            $Validacion->Requerido('Piso', '* Campo Requerido');
            $Validacion->Requerido('Numero', '* Campo Requerido');
            $Validacion->Requerido('Status', '* Campo Requerido');
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
            $Plantilla->Parametro('IdHotel', $IdHotel);
            $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarCuarto'));
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel','Cuartos', 'Agregar', 'frmAgregar.html')));
            unset($Instructores, $Periodos, $Validacion, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * AgregarCuarto()
     *
     * Funcion de agregar Cuarto
     * @throws NeuralException
     */
    public function AgregarCuarto()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {
                if (AppPost::DatosVaciosOmitidos($_POST, array('Nombre')) == false AND AppPost::DatosVaciosOmitidos($_POST, array('Status')) == false) {
                    $IdHotel = NeuralCriptografia::DeCodificar($_POST['IdHotel'], APP);
                    if (isset($_POST['Nombre']) == true)
                        $Nombres = $_POST['Nombre'];

                    if (isset($_POST['Status2']) == true)
                        $Status = $_POST['Status2'];
                    unset($_POST['Key'], $_POST['Nombre'], $_POST['Status2']);

                    $DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
                    $DatosPost['IdHotel'] = NeuralCriptografia::DeCodificar($DatosPost['IdHotel'], APP);

                    $IdCuarto = $this->Modelo->GuardarCuartos($DatosPost);
                    if (isset($IdCuarto) == true AND isset($Nombres) == true AND is_array($Nombres) == true AND count($Nombres) > 0 AND isset($Status) == true AND is_array($Status) == true AND count($Status) > 0) {
                        $PosicionStatus = current($Status);
                        foreach ($Nombres as $Nombre) {
                            $this->Modelo->GuardarCamas(array(
                                'IdCuarto' => $IdCuarto,
                                'Nombre' => $Nombre,
                                'Status' => $PosicionStatus));
                            $PosicionStatus = next($Status);
                        }
                    }
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    $Plantilla->Parametro('IdHotel', $IdHotel);
                    $Plantilla->Filtro('Cifrado', function ($Parametro) {
                        return NeuralCriptografia::Codificar($Parametro, APP);
                    });
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Cuartos', 'Agregar', 'Exito.html')));
                    unset($Nombres, $Status, $DatosPost, $IdCuarto, $Nombre, $PosicionIdDia, $Plantilla);
                    exit();
                } else {
                    // -- Mensaje de error al insertar datos
                    $Plantilla = new NeuralPlantillasTwig(APP);
                    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Error', 'ErrorInsertandoDatos.html')));
                    unset($IdAuxiliares, $IdInstructor, $DatosPost, $IdTaller, $Plantilla);
                    exit();
                }
            } else {
                // -- Mensaje elementos requeridos para la captura
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Error', 'ErrorElementosRequeridos.html')));
                unset($Plantilla);
                exit();
            }
        }
    }



    /**
     * Metodo publico
     * frmEditarCuarto()
     *
     * Formulario para editar un Cuarto.
     * @throws NeuralException
     */
    public function frmEditarCuarto()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true and $_POST['IdCuarto'] != '') {
                $IdCuarto = NeuralCriptografia::DeCodificar($_POST['IdCuarto'], APP);
                $IdHotel = NeuralCriptografia::DeCodificar($_POST['IdHotel'], APP);
                $Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
                $Consulta = $this->Modelo->ConsultarCuartoUnico($IdCuarto);
                $ConsultarCamas = $this->Modelo->ConsultarCamas($IdCuarto);
                $Validacion->Requerido('Piso', '* Campo Requerido');
                $Validacion->Requerido('Numero', '* Campo Requerido');
                $Validacion->Requerido('Status', '* Campo Requerido');
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('IdHotel', $IdHotel);
                $Plantilla->Parametro('ConsultaCama', $ConsultarCamas);
                $Plantilla->Parametro('Key', NeuralCriptografia::Codificar(AppFechas::ObtenerFechaActual(), APP));
                $Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarCuarto'));
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Cuartos', 'Editar', 'frmEditarCuarto.html')));
                unset($Instructores, $Periodos, $Validacion, $Plantilla);
                exit();
            }
        }
    }

    /**
     * Metodo Publico
     * EditarCuarto()
     *
     * Funcion Editar Cuarto
     * @throws NeuralException
     */
    public function EditarCuarto(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar($_POST['Key'], APP) == AppFechas::ObtenerFechaActual()) == true) {

                if (isset($_POST['Nombre']) == true)//se verifica que no venga vacio si viene basio simplemente no se crea la variable
                    $DatosCamas=$_POST['Nombre'];
                if (isset($_POST['Status2']) == true)
                    $DatosCamasStatus=$_POST['Status2'];

                unset($_POST['Nombre'], $_POST['Status2'], $_POST['Key']);
                $DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
                $IdHotel = NeuralCriptografia::DeCodificar($DatosPost['IdHotel'], APP);
                unset($DatosPost['IdHotel']);
                $DatosPost['IdCuarto'] = NeuralCriptografia::DeCodificar($DatosPost['IdCuarto'], APP);
                unset($_POST);

                $this->Modelo->EliminarCamas($DatosPost['IdCuarto']);

                if (isset($DatosCamas) == true and isset($DatosCamasStatus) == true) {
                    $PosicionStatus = current($DatosCamasStatus);
                    foreach ($DatosCamas as $Nombre) {
                        $this->Modelo->GuardarCamas(array(
                            'IdCuarto' => $DatosPost['IdCuarto'],
                            'Nombre' => $Nombre,
                            'Status' => $PosicionStatus));
                        $PosicionStatus = next($DatosCamasStatus);
                    }
                }

                $this->Modelo->ActualizaCuarto(array(
                    'Numero' =>$DatosPost['Numero'],
                    'Piso' =>$DatosPost['Piso'],
                    'Status' =>$DatosPost['Status']), array(
                    'IdCuarto' =>$DatosPost['IdCuarto']));

                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('IdHotel', $IdHotel);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });

                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel','Cuartos', 'Editar', 'Exito.html')));
                unset($Plantilla, $DatosPost, $DatosCamas, $DatosCamasStatus);
                exit();
            } else {
                $Plantilla = new NeuralPlantillasTwig(APP);
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Hotel', 'Error', 'ErrorElementosRequeridos.html')));
                unset($Plantilla);
                exit();
            }
        }
    }


    /**
     * Metodo Publico
     * EliminarCuarto()
     *
     * Funcion Eliminar Cuarto
     */
    public function EliminarCuarto()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if (isset($_POST) == true and $_POST['IdCuarto'] != '') {
                $IdCuarto = NeuralCriptografia::DeCodificar($_POST['IdCuarto'], APP);
                $this->Modelo->EliminarCuarto($IdCuarto);
            }
        }

    }

}