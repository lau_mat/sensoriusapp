<?php
class Sensor_Modelo extends AppSQLConsultas  {

    /**
     * Metodo: Constructor
     */
    function __Construct() {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarSensores()
     *
     * Consulta y retorna a los Sensores que esten activos
     * dentro de la Base de Datos
     */
    public function ConsultarSensores() {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_sensores');
        $Consulta->Columnas("Mac,tbl_sensores.Status,tbl_sensores.Nombre as N_Sensor, tbl_camas.Nombre as N_Cama, tbl_cuartos.Piso, tbl_cuartos.Numero");
        $Consulta->InnerJoin('tbl_camas', 'tbl_sensores.IdCama', 'tbl_camas.IdCama');
        $Consulta->InnerJoin('tbl_cuartos', 'tbl_camas.IdCuarto', 'tbl_cuartos.IdCuarto');
        $Consulta->Condicion("tbl_sensores.Status!= 'ELIMINADO'");
        return $Consulta->Ejecutar(false,true);
    }

    /**
     * Metodo Publico
     * ConsultarPiso()
     *
     * Consulta y retorna a los pisos y que aparte estos esten activos
     * dentro de la Base de Datos
     */
    public function ConsultarPiso() {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_cuartos');
        $Consulta->Columnas("DISTINCT Piso");
        $Consulta->Condicion("tbl_cuartos.Status != 'ELIMINADO'");
        return $Consulta->Ejecutar(false,true);
    }

    /**
     * Metodo Publico
     * ConsultarCuartos()
     *
     * Consulta y retorna a Cuartos de dicho piso
     * dentro de la Base de Datos
     */
    public function ConsultarCuartos($Piso= false) {
        if($Piso== true and $Piso != '') {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_cuartos');
            $Consulta->Columnas("IdCuarto, Numero");
            $Consulta->Condicion("tbl_cuartos.Piso='" . $Piso . "' AND tbl_cuartos.Status != 'ELIMINADO'");
            return $Consulta->Ejecutar(false, true);
        }
    }


    /**
     * Metodo Publico
     * ConsultarCuartos()
     *
     * Consulta y retorna a Cuartos de dicho piso
     * dentro de la Base de Datos
     */
    public function ConsultarCamas($IdCuarto= false) {
        if($IdCuarto == true and $IdCuarto != '') {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_camas');
            $Consulta->Columnas("IdCama, Nombre");
            $Consulta->Condicion("tbl_camas.IdCuarto='" . $IdCuarto . "' AND tbl_camas.Status != 'ELIMINADO'");
            return $Consulta->Ejecutar(false, true);
        }
    }

    /**
     * @param var $Datos
     * Metodo Publico InsertarSensor
     *
     * Recibe un arreglo para registrar un sensor
     */
    public function InsertarSensor($Datos){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->insert('tbl_sensores', $Datos);
            } catch (PDOException $e){} catch (Exception $e) {}
        }
    }


    /**
     * Metodo Publico
     * Eliminar()
     *
     * Metodo Publico Eliminar Sensor
     * Cambia el Estado  del sensor
     * a ELIMINADO
     */
    public function Eliminar($Mac= false){
        if($Mac== true and $Mac != ''){
            try{
                $this->Conexion->update('tbl_sensores',array('Status'=>"ELIMINADO"),array('Mac'=>$Mac));
            }catch (PDOException $e){
            }catch (Exception $e){}

        }
    }
   /**
     * Metodo Publico
     * ActualizaSensorMac()
     *
     * Metodo Publico actualiza datos del Sensor
     *
     */
    public function ActualizaSensor($Datos= false, $Condicion){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->update('tbl_sensores', $Datos, $Condicion);
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    /**
     * Metodo Publico
     * ConsultarSensorUnico()
     *
     * Consulta y retorna la informacion del Sensor seleccionado
     */
    public function ConsultarSensorUnico($Mac= false) {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_sensores');
        $Consulta->Columnas("Mac,tbl_sensores.Status,tbl_sensores.Nombre as N_Sensor, tbl_camas.Nombre as N_Cama, tbl_camas.IdCama, tbl_cuartos.Piso, tbl_cuartos.Numero, tbl_cuartos.IdCuarto");
        $Consulta->InnerJoin('tbl_camas', 'tbl_sensores.IdCama', 'tbl_camas.IdCama');
        $Consulta->InnerJoin('tbl_cuartos', 'tbl_camas.IdCuarto', 'tbl_cuartos.IdCuarto');
        $Consulta->Condicion("tbl_sensores.Status!= 'ELIMINADO'  AND tbl_sensores.Mac = '". $Mac ."'");
        return $Consulta->Ejecutar(false,true);
    }

}