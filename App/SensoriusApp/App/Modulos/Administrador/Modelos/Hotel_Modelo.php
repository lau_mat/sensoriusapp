<?php
class Hotel_Modelo extends AppSQLConsultas  {

    /**
     * Metodo: Constructor
     */
    function __Construct() {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarHoteles()
     *
     * Consulta y retorna a los hoteles almancenados en la bd
     * dentro de la Base de Datos
     */
    public function ConsultarHoteles() {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_hoteles');
        $Consulta->Columnas("IdHotel, Nombre, Direccion, RutaWebImagen");
        $Consulta->Condicion("Status != 'ELIMINADO'");
        return $Consulta->Ejecutar(false,true);
    }

    /**
     * Metodo Publico
     * ConsultarCuartos()
     *
     * Consulta y retorna a los usuarios de perfil Instructor y que aparte estos esten activos
     * dentro de la Base de Datos
     */
    public function ConsultarCuartos($IdHotel = false) {
        if($IdHotel==true){
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_cuartos');
        $Consulta->Columnas("Numero,Piso,Status,IdCuarto");
        $Consulta->Condicion("tbl_cuartos.Status!= 'ELIMINADO'");
        $Consulta->Condicion("IdHotel=".$IdHotel);
        return $Consulta->Ejecutar(false,true);
             }
    }

    /**
     * Metodo Publico
     *  ConsultarCamas($IdCuarto= false)
     *
     * Consulta las camas del cuarto seleccionado
     * @param bool $Condicion
     * @return mixed
     */
    public function ConsultarCamas($IdCuarto = false){
        $Campos="IdCama,Nombre,Status";
        $SQL = "SELECT $Campos FROM tbl_camas";
        $SQL.= " WHERE Status!= 'ELIMINADO' AND IdCuarto = $IdCuarto";
        $Consulta = $this->Conexion->prepare($SQL);
        $Consulta->execute();
        return $Consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Metodo Publico
     * ConsultarCuartoUnico()
     *
     * Consulta y retorna la informacion del Sensor seleccionado
     */
    public function ConsultarCuartoUnico($IdCuarto= false) {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_cuartos');
        $Consulta->Columnas("IdCuarto,Numero,Piso,Status");
        $Consulta->Condicion("tbl_cuartos.Status!= 'ELIMINADO' AND tbl_cuartos.IdCuarto = $IdCuarto");
        return $Consulta->Ejecutar(false,true);
    }

    /**
     * Metodo Publico
     * GuardarHotel($ArregloDatos = false)
     *
     * Guarda el Hotel
     * @param bool $Datos: Arreglo de datos, DatosPost
     * @return mixed
     */
    public function GuardarHotel($Datos = false){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->insert('tbl_hoteles', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * GuardarCamas($ArregloDatos = false)
     *
     * Guarda las Camas del cuarto Correspondiente
     * @param bool $Datos: Arreglo de datos, DatosPost
     * @return mixed
     */
    public function GuardarCamas($Datos = false){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->insert('tbl_camas', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * GuardarCuartos($ArregloDatos = false)
     *
     * Guarda el cuarto
     * @param bool $Datos: Arreglo de datos, DatosPost
     * @return mixed
     */
    public function GuardarCuartos($Datos = false){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->insert('tbl_cuartos', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e) {
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Metodo Publico
     * ActualizaCuarto()
     *
     * Metodo Publico actualiza datos de un Cuarto
     *
     */
    public function ActualizaCuarto($Datos= false, $Condicion){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->update('tbl_cuartos', $Datos, $Condicion);
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    /**
     * Metodo Publico
     * EliminarCuarto()
     *
     * Metodo Publico Eliminar Cuarto
     * Cambia el Estado  del Cuarto
     * a ELIMINADO
     */
    public function EliminarCuarto($IdCuarto= false){
        if($IdCuarto== true and $IdCuarto!= ''){
            try{
                $this->Conexion->update('tbl_cuartos',array('Status'=>"ELIMINADO"),array('IdCuarto'=>$IdCuarto));
            }catch (PDOException $e){
            }catch (Exception $e){}

        }
    }

    /**
     * Metodo Publico
     * EliminarCamas()
     *
     * Metodo Publico Eliminar Camas
     * Cambia el Estado de la cama
     * a ELIMINADO
     */

    public function EliminarCamas($IdCuarto= false){
        if($IdCuarto== true and $IdCuarto!= ''){
            try{
                $this->Conexion->update('tbl_camas',array('Status'=>"ELIMINADO"),array('IdCuarto'=>$IdCuarto));
            }catch (PDOException $e){
            }catch (Exception $e){}

        }
    }



}