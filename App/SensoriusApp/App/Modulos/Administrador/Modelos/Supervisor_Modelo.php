<?php
class Supervisor_Modelo extends AppSQLConsultas  {

    /**
     * Metodo: Constructor
     */
    function __Construct() {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultarSupervisores()
     *
     * Consulta y retorna a los usuarios de perfil Instructor y que aparte estos esten activos
     * dentro de la Base de Datos
     */
    public function ConsultarSupervisores() {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_usuarios');
        $Consulta->Columnas("IdUsuario, Nombres, Ap_Paterno, Ap_Materno, tbl_usuarios.Status as Status, Email, tbl_hoteles.IdHotel, tbl_hoteles.Nombre");
        $Consulta->InnerJoin('tbl_personas', 'tbl_usuarios.IdPersona', 'tbl_personas.IdPersona');
        $Consulta->InnerJoin('tbl_sistema_usuarios_perfil', 'tbl_usuarios.IdPerfil', 'tbl_sistema_usuarios_perfil.IdPerfil');
        $Consulta->InnerJoin('tbl_hoteles', 'tbl_usuarios.IdHotelAsignado', 'tbl_hoteles.IdHotel');
        $Consulta->Condicion("tbl_sistema_usuarios_perfil.Nombre='Supervisor'");
        $Consulta->Condicion("tbl_usuarios.Status!= 'ELIMINADO'");
        return $Consulta->Ejecutar(false,true);
    }

    /**
     * Metodo Publico
     * ConsultarHoteles()
     *
     * Consulta y retorna a los hoteles y que aparte estos esten activos
     * dentro de la Base de Datos
     */
    public function ConsultarHoteles() {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_hoteles');
        $Consulta->Columnas("IdHotel, Nombre");
        $Consulta->Condicion("Status!= 'ELIMINADO'");
        return $Consulta->Ejecutar(false,true);
    }

    /**
     * @param var $Usuario
     * Metodo Publico BuscarUsuario
     * Recibe el nombre del usuario para buscar si este esta registrado y devuelve el Id
     * */
    public function BuscarUsuario($Usuario=false){
        if(isset($Usuario) == true AND $Usuario != ""){
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_usuarios');
            $Consulta->Columnas("tbl_usuarios.IdUsuario");
            $Consulta->Condicion("tbl_usuarios.Usuario = '".$Usuario."'");
            return $Consulta->Ejecutar(false,true);
        }
    }

    /**
     * @param array $Array, var $IdUsuario
     * Metodo Publico InsertarInformacionUsuario
     * Registra la informacion de un usuario regresa un Id
     * */
    public function InsertarUsuario($Array = false, $IdUsuario=false){
        if($Array == true AND $IdUsuario == true){
            $SQL = new NeuralBDGab(APP, 'tbl_usuarios');
            $SQL->Sentencia('IdPersona',$IdUsuario);
            foreach ($Array as $key => $Valor){
                $SQL->Sentencia($key, $Valor);
            }
            $SQL->Insertar();
        }
    }

    /**
     * @param var $Datos
     * Metodo Publico InsertarPersona
     * Recibe un arreglo para agregar una persona
     * */
    public function InsertarPersona($Datos){
        if($Datos == true AND is_array($Datos) == true){
            try{
                $this->Conexion->insert('tbl_personas', $Datos);
                return $this->Conexion->lastInsertId();
            } catch (PDOException $e){} catch (Exception $e) {}
        }
    }

    /**
     * Metodo Publico
     * Eliminar()
     *
     * Metodo Publico Eliminar Supervisor
     * Cambia el Estado  del supervisor
     * a ELIMINADO
     */
    public function Eliminar($IdUsuario= false){
        if($IdUsuario== true and $IdUsuario!= ''){
            try{
                $this->Conexion->update('tbl_usuarios',array('Status'=>"ELIMINADO"),array('IdUsuario'=>$IdUsuario));
            }catch (PDOException $e){
            }catch (Exception $e){}

        }
    }

    /**
     * @param var $IdUsuario
     * Metodo Publico ConsultarInformacionUsuario
     * Recupera la informacion de un usuario con un determinado Id
     * */
    public function ConsultarInformacionUsuario($IdUsuario=false){
        if(isset($IdUsuario)==true AND $IdUsuario!=""){
            $Consulta=new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_usuarios');
            $Consulta->Columnas("IdUsuario, Usuario, Password, tbl_personas.IdPersona, Nombres, Ap_Paterno, Ap_Materno, Email, IdHotel, Nombre");
            $Consulta->InnerJoin('tbl_personas','tbl_usuarios.IdPersona','tbl_personas.IdPersona');
            $Consulta->InnerJoin('tbl_hoteles', 'tbl_usuarios.IdHotelAsignado', 'tbl_hoteles.IdHotel');
            $Consulta->Condicion('tbl_usuarios.IdUsuario = '.$IdUsuario);
            return $Consulta->Ejecutar(false,true);
        }
    }

    /**
     * @param bool $Datos
     * @param bool $IdUsuario
     *
     * Metodo publico
     * ActualizarDatosUsuario()
     *
     * Recibe  un array con el Status del usuario y su id para modificarlo en la DB
     */
    public function ActualizarDatosUsuario($Datos = false, $IdUsuario = false){
        if($IdUsuario == true AND $IdUsuario != '' AND is_array($Datos) == true){
            try{
                $this->Conexion->update('tbl_usuarios', $Datos, array('IdUsuario'=>$IdUsuario));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

    public function ActualizarDatosPersona($Datos = false, $IdPersona = false){
        if($IdPersona == true AND $IdPersona!= '' AND is_array($Datos) == true){
            try{
                $this->Conexion->update('tbl_personas', $Datos, array('IdPersona'=>$IdPersona));
            } catch (PDOException $e) {
            } catch (Exception $e) {}
        }
    }

}