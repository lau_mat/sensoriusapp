<?php
class Actividad_Modelo extends AppSQLConsultas
{

    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultaActividad()
     *
     * Consulta y retorna a los SENSORES con su ACTIVIDAD
     */
    public function ConsultaActividad()
    {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_cuartos');
        $Consulta->Columnas("tbl_cuartos.IdCuarto, Piso, Numero, tbl_camas.Nombre, tbl_sensores.Mac, tbl_camas.Status");
        $Consulta->InnerJoin('tbl_camas', 'tbl_cuartos.IdCuarto', 'tbl_camas.IdCuarto');
        $Consulta->InnerJoin('tbl_sensores', 'tbl_camas.IdCama', 'tbl_sensores.IdCama');
        $Consulta->Condicion("tbl_camas.Status!= 'ELIMINADO'");
        return $Consulta->Ejecutar(false,true);
    }

    /**
     * Metodo Publico
     * ConsultaActividad()
     *
     * Consulta y retorna a los SENSORES con su ACTIVIDAD
     */
    public function ConsultaActividadDetalle($Mac = false)
    {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_cuartos');
        $Consulta->Columnas("tbl_cuartos.IdCuarto, Piso, Numero, tbl_camas.Nombre, tbl_sensores.Mac, tbl_camas.Status");
        $Consulta->InnerJoin('tbl_camas', 'tbl_cuartos.IdCuarto', 'tbl_camas.IdCuarto');
        $Consulta->InnerJoin('tbl_sensores', 'tbl_camas.IdCama', 'tbl_sensores.IdCama');
        $Consulta->Condicion("tbl_camas.Status!= 'ELIMINADO' and tbl_sensores.Mac= '$Mac'");
        return $Consulta->Ejecutar(false,true);
    }
    /**
     * Metodo Publico
     * ConsultaActividadMac()
     *
     * Consulta y retorna a las actividades de un Sensor dependiendo de la Mac
     */
    public function ConsultaActividadMac($Mac = false)
    {
        if($Mac == true and $Mac != '') {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_registros');
            $Consulta->Columnas("Mac, Hora, Fecha, Status");
            $Consulta->Condicion("Mac= '$Mac' Order by Fecha desc, Hora desc");
            return $Consulta->Ejecutar(false,true);
        }
    }
}