<?php
class Demo_Modelo extends AppSQLConsultas
{
    /**
     * Metodo: Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        $this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
    }

    /**
     * Metodo Publico
     * ConsultaActividad()
     *
     * Consulta y retorna a los SENSORES con su ACTIVIDAD
     */
    public function ConsultaActividad()
    {
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_cuartos');
        $Consulta->Columnas("tbl_cuartos.IdCuarto, Piso, Numero, tbl_camas.Nombre, tbl_sensores.Mac, tbl_camas.Status");
        $Consulta->InnerJoin('tbl_camas', 'tbl_cuartos.IdCuarto', 'tbl_camas.IdCuarto');
        $Consulta->InnerJoin('tbl_sensores', 'tbl_camas.IdCama', 'tbl_sensores.IdCama');
        $Consulta->Condicion("tbl_camas.Status!= 'ELIMINADO'");
        return $Consulta->Ejecutar(false, true);
    }

    public function ListaMac(){
        $Consulta = new NeuralBDConsultas(APP);
        $Consulta->Tabla('tbl_sensores');
        $Consulta->Columnas("Mac");
        $Consulta->Condicion("Status!= 'ELIMINADO'");
        return $Consulta->Ejecutar(false, true);
    }

    /**
     * Metodo Publico
     * ConsultaActividadMac()
     * @param bool $Mac
     *
     * Consulta y retorna a las actividades de un Sensor dependiendo de la Mac
     */
    public function ConsultaActividadMac($Mac = false)
    {
        if($Mac == true and $Mac != '') {
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_registros');
            $Consulta->Columnas("Mac, Hora, Fecha, Status");
            $Consulta->Condicion("Mac= '$Mac' Order by Fecha desc, Hora desc");
            return $Consulta->Ejecutar(false, true);
        }
    }

    /**
     * Metodo Publico
     * InsertarRegistro($Mac =false, $StatusNew = false)
     * @param bool $Mac
     * @param bool $StatusNew
     *
     * Guarda un nuevo Registro asociado a la Mac
     */
    public function InsertarRegistro($Mac =false, $StatusNew = false){

        if($Mac == true and $Mac != '' and  $StatusNew == true and $StatusNew !=''){
            $Fecha = AppFechas::ObtenerFechaActual();
            $Hora = AppFechas::ObtenerHoraActual();
            try{
                $this->Conexion->insert('tbl_registros', array('Mac'=>$Mac, 'Hora'=>$Hora, 'Fecha'=>$Fecha, 'Status'=>$StatusNew));
            } catch (PDOException $e){
            } catch (Exception $e) {

            }
        }
    }

    /**
     * Metodo Publico
     * ActualizaStatusCama($IdCama = false, $StatusNew = false);
     * @param bool $IdCama
     * @param bool $StatusNew
     *
     * Este metodo Actualiza el estado Actual de la Cama el cual
     * utilizamos como Estudo de tiempo real
     */
    public function ActualizaStatusCama($IdCama = false, $StatusNew = false){
        if($IdCama == true and $IdCama != '' and  $StatusNew == true and $StatusNew !=''){
            try{
                $this->Conexion->update('tbl_camas',array('Status'=>$StatusNew),array('IdCama'=>$IdCama));
            }catch (PDOException $e){
            }catch (Exception $e){}
        }
    }

    /**
     * Metodo Publico
     * BuscaIdCama($Mac = false)
     * @param bool $Mac
     *
     * En este metodo retornamos el IdCama que esta relacionado con la Mac
     */
    public function BuscaIdCama($Mac = false){
        if($Mac == true and $Mac != ''){
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_sensores');
            $Consulta->Columnas("IdCama");
            $Consulta->Condicion("Mac= '$Mac' and Status != 'ELIMINADO'");
            return $Consulta->Ejecutar(false, true);
        }
    }

    /**
     * Metodo Publico
     * ProcesaDemo($Mac =false, $StatusNew = false);
     * @param bool $Mac
     * @param bool $StatusNew
     *
     * Este metodo decide si crear un nuevo registro o no
     */
    public function ProcesaDemo($Mac =false, $StatusNew = false){
        if($Mac == true and $Mac != '' and  $StatusNew == true and $StatusNew !=''){
            if($this->ChecarAnterior($Mac, $StatusNew)=='StatusNew'){

               $IdCama = $this->BuscaIdCama($Mac);
               $this->InsertarRegistro($Mac, $StatusNew);

               $IdCama = $IdCama[0]['IdCama'];
               $this->ActualizaStatusCama($IdCama, $StatusNew);
               unset($IdCama);
            }
        }
        unset($Mac, $StatusNew);
    }

    /**
     * Metodo Publico
     * ChecarAnterior($Mac =false, $StatusNew = false);
     * @param bool $Mac
     * @param bool $StatusNew
     *
     * Metodo que compara el nuevo estado del sensor con el ultimo
     * que fue registrado
     */
    public function ChecarAnterior($Mac =false, $StatusNew = false){
        if($Mac == true and $Mac != '' and  $StatusNew == true and $StatusNew !=''){
            $Fecha = AppFechas::ObtenerFechaActual();
            $Consulta = new NeuralBDConsultas(APP);
            $Consulta->Tabla('tbl_registros');
            $Consulta->Columnas("Status");
            $Consulta->Condicion("Mac = '$Mac' and Fecha = '$Fecha' order by IdRegistro desc Limit 1");
            $StatusOld = $Consulta->Ejecutar(true, true);

            if($StatusOld['Cantidad'] == 1){

                unset($StatusOld['Cantidad']);
                $StatusOld = $StatusOld[0]['Status'];

                if($StatusOld == $StatusNew){
                    return "StatusOld";
                }else{
                    return "StatusNew";
                }
            }else{
                return "StatusNew";
            }
        }
    }
}