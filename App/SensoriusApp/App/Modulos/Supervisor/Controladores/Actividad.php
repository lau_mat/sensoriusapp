<?php
class Actividad extends Controlador
{

    var $Informacion;
    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }
    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['Ap_Paterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Actividad', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }


    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista todos Supervisores registrados en la db
     */
    public function frmListado()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultaActividad();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Actividad', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * ListadoActividad()
     *
     * Lista todos registros de un Sensor idenditificado por su Mac
     */
    public function ListadoActividad()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if(isset($_POST)== true and $_POST['Mac'] != ''){
                $Mac= NeuralCriptografia::DeCodificar($_POST['Mac'], APP);
                $Consulta = $this->Modelo->ConsultaActividadMac($Mac);
                $ConsultaDetalle= $this->Modelo->ConsultaActividadDetalle($Mac);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Parametro('ConsultaDetalle', $ConsultaDetalle);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Actividad', 'ListadoActividad', 'ListadoActividad.html')));
                unset($Consulta, $Plantilla);
                exit();

            }
        }
    }

}