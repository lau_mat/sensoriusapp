<?php
class Reporte extends Controlador
{
    var $Informacion;

    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }

    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index(){
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['Ap_Paterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Reporte', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }

    /**
     * Metodo Publico
     * frmOpciones()
     *
     * Mostramos las diferentes opciones de reportes
     */
    public function frmOpciones(){
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Plantilla = new NeuralPlantillasTwig(APP);
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Reporte', 'Opciones', 'Opciones.html')));

        }
    }

    /**
     * Metodo Publico
     * R_Diario()
     *
     * Se genera una plantilla que mandamos a PDF
     */
    public function R_Diario(){
        $rutaCss = implode(DIRECTORY_SEPARATOR,array(__SysNeuralFileRootApp__, "SensoriusApp","Web", "css", "stylePDF.css"));
        $Style = file_get_contents($rutaCss);
        $Plantilla = new NeuralPlantillasTwig(APP);
       // $Plantilla->Parametro('Consulta', 2000);
        $Contenido = $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Reporte','ReportesPDF', 'Diario.html')));
        $app = new AppPdf();
        $app->EstablecerStyle($Style);
        $app->GenerarPdf($Contenido);
    }

}