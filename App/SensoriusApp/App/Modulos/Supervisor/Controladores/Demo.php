<?php
class Demo extends Controlador
{

    var $Informacion;
    /**
     * Metodo Constructor
     */
    function __Construct()
    {
        parent::__Construct();
        AppSession::ValSessionGlobal();
        $this->Informacion = AppSession::InfomacionSession();
    }
    /**
     * Metodo Publico
     * Index()
     *
     * Pantalla Principal del sistema
     *
     */
    public function Index()
    {
        $MenuSeleccion = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
        $MenuSeleccion = (isset($MenuSeleccion[2])) ? $MenuSeleccion[2] : 'Index';
        $TipoUsuario = $this->Informacion['Permiso']['Nombre'];
        $Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['Ap_Paterno'];
        $Plantilla = new NeuralPlantillasTwig(APP);
        $Plantilla->Parametro('TipoUsuario', $TipoUsuario);
        $Plantilla->Parametro('Menu', $MenuSeleccion);
        $Plantilla->Parametro('Usuario', $Usuario);
        echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Demo', 'Index.html')));
        unset($MenuSeleccion, $TipoUsuario, $Usuario, $Plantilla);
        exit();
    }


    /**
     * Metodo Publico
     * frmListado()
     *
     * Lista todos Supervisores registrados en la db
     */
    public function frmListado()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            $Consulta = $this->Modelo->ConsultaActividad();
            $Plantilla = new NeuralPlantillasTwig(APP);
            $Plantilla->Parametro('Consulta', $Consulta);
            $Plantilla->Filtro('Cifrado', function ($Parametro) {
                return NeuralCriptografia::Codificar($Parametro, APP);
            });
            echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Demo', 'Listado', 'Listado.html')));
            unset($Consulta, $Plantilla);
            exit();
        }
    }

    /**
     * Metodo Publico
     * ListadoActividad()
     *
     * Lista todos registros de un Sensor idenditificado por su Mac
     */
    public function ListadoActividad()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']) {
            if(isset($_POST)== true and $_POST['Mac'] != ''){
                $Mac= NeuralCriptografia::DeCodificar($_POST['Mac'], APP);
                $Consulta = $this->Modelo->ConsultaActividadMac($Mac);
                $Plantilla = new NeuralPlantillasTwig(APP);
                $Plantilla->Parametro('Consulta', $Consulta);
                $Plantilla->Filtro('Cifrado', function ($Parametro) {
                    return NeuralCriptografia::Codificar($Parametro, APP);
                });
                echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Demo', 'ListadoActividad', 'ListadoActividad.html')));
                unset($Consulta, $Plantilla);
                exit();

            }
        }
    }

    /**Metodo Publico
     * Random()
     *
     * Genera Estados aleatorios para los sensores
     */
    public function Random(){
        $MacGuardadas = $this->Modelo->ListaMac();
        foreach($MacGuardadas as $Dato ){
            $Mac = $Dato['Mac'];
            $StatusNew = $this->GenerarEstado(rand(1, 3));
            $this->Modelo->ProcesaDemo($Mac, $StatusNew);
        }
        $this->frmListado();
        unset($MacGuardadas, $Dato, $Mac, $StatusNew);
    }

    /**
     * Metodo publco
     * GenerarEstado($num = false)
     * @param bool $num
     * @return string
     *
     * Nos devuelve un estado dependiendo del caso
     */
    public function GenerarEstado($num = false){
        switch ($num){
            case 1:
              return "Online";
            case 2:
                return "Offline";
            case 3:
                return "Ocupado";
        }
    }
}